package edu.serene.extraction;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ListBranchCommand.ListMode;
import org.eclipse.jgit.errors.CorruptObjectException;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.treewalk.AbstractTreeIterator;
import org.eclipse.jgit.treewalk.TreeWalk;

import com.github.javaparser.ParseException;

import edu.serene.bean.CommitBean;
import edu.serene.bean.IssueBean;

public class GitCommits {

	public static void main(String[] args) throws IOException {
		File repoDir = new File("C:/Users/Chris/Latest/JMetricLite/.git");
		
		FileRepositoryBuilder builder = new FileRepositoryBuilder();
		try (Repository repo = builder.setGitDir(repoDir).readEnvironment().findGitDir().build()) {
			try(Git git = new Git(repo)) {
				for(Ref r : git.branchList().setListMode(ListMode.ALL).call()) {
					System.out.println(r.getName());
				}
				
				Iterable<RevCommit> logs = git.log().call();
				
				CommitBean prevCommit = null;
				for(RevCommit rev : logs) {
					//System.out.println("Commit: " + rev  + ", name: " + rev.getName() + ", id: " + rev.getId().getName() + ", message: " + rev.getShortMessage());
					CommitBean commit = new CommitBean(rev.getName());
					
					compareCommits(prevCommit, commit, repo);
					prevCommit = commit;
				}
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private static void compareCommits(CommitBean commitA, CommitBean commitB, Repository repo) throws MissingObjectException, IncorrectObjectTypeException, IOException, ParseException {
		if(commitA == null || commitB == null) {
			return;
		}
		//System.out.println(commitA.getSha() + " -> " + commitB.getSha());
		System.out.println("\nCommit : " + commitA.getSha());
		RevWalk revWalk = new RevWalk(repo);
		
		RevTree treeA = revWalk.parseCommit(ObjectId.fromString(commitA.getSha())).getTree();
		RevTree treeB = revWalk.parseCommit(ObjectId.fromString(commitB.getSha())).getTree();
		
		//use commitA as a reference.//TODO this is pretty exhaustive... need to think about a more reasonable way of comparing the two branches
		HashMap<ObjectId, String> setA = decomposeTree(repo, treeA);
		HashMap<ObjectId, String> setB = decomposeTree(repo, treeB);
		
		for(ObjectId id : setA.keySet()) {
			String pathA = setA.get(id);
			String pathB = setB.get(id);
			if(!pathA.equalsIgnoreCase(pathB)) {
				//TODO if they are different, get the loaders and then print the commits to temp files and compare them using JavaParser
				//http://www.doublecloud.org/2013/01/how-to-read-git-repository-using-java-apis/ --this will get you started on writing out the temp files.
				File compUnitA = generateTempFile(repo.open(id), pathA, commitA.getSha(), null);
				File compUnitB = null;
				ObjectId updatedId = reverseSearchHashMap(setB, pathA);
				if(updatedId != null) {
					compUnitB = generateTempFile(repo.open(updatedId), pathA, commitB.getSha(), getUUIDFromTempPath(compUnitA.getName()));
					
					CompilationUnitComparer cuCompare = new CompilationUnitComparer(compUnitA, compUnitB);
					for(String s : cuCompare.changedMethods) {
						System.out.println(s);
					}
				} else {
					System.out.println("Warning: Compilation unit " + pathA + " was added in commit " + commitA.getSha() + ". GitData does not currently support this method of modification!");
					//TODO: support adding/deletign methods from comp units.
				}
				
			}
		}
		
		revWalk.close();
	}
	
	private static String getUUIDFromTempPath(String tmpPath) {
		return tmpPath.split("\\.")[1];
	}
	
	private static ObjectId reverseSearchHashMap(HashMap<ObjectId, String> hash, String revKey) {
		for(Entry<ObjectId, String> entry : hash.entrySet()) {
			if(entry.getValue().equalsIgnoreCase(revKey)) {
				return entry.getKey();
			}
		}
		
		return null;
	}
	
	private static File generateTempFile(ObjectLoader loader, String cuName, String commitId, String uniqueId) throws MissingObjectException, IOException {
		//use the cu (compilation unit) name as a root for the temp file name. This makes it easier to debug problems. 
		//if the cu name contains "/"s, just take the end
		String tmpRoot = cuName;
		if(tmpRoot.contains("/")) {
			String[] tok = tmpRoot.split("/");
			tmpRoot = tok[tok.length - 1];
		}
		//generate a random guid and that will be the file name
		String uniqueFileId = "";
		if(uniqueId == null) {
			uniqueFileId = UUID.randomUUID().toString();
		} else {
			uniqueFileId = uniqueId;
		}
		
		File tmpFile = new File(commitId + "." + uniqueFileId + "." + tmpRoot + ".txt");
		if(tmpFile.exists()) {
			//if the universe breaks and for some reason we have the same name as a previous file, we should probably just blow it up.
			tmpFile.delete();
		}
		FileOutputStream fileA = new FileOutputStream(tmpFile);
		loader.copyTo(fileA);
		
		fileA.close();
		
		return tmpFile;
	}
	
	private static HashMap<ObjectId, String> decomposeTree(Repository repo, RevTree ati) throws MissingObjectException, IncorrectObjectTypeException, CorruptObjectException, IOException {
		TreeWalk treeWalk = new TreeWalk(repo);
		treeWalk.addTree(ati);
		treeWalk.setRecursive(false);
		
		HashMap<ObjectId, String> set = new HashMap<ObjectId, String>();
		
		while(treeWalk.next()) {
			if(treeWalk.isSubtree()) {
				treeWalk.enterSubtree();
			} else {
				//System.out.println("file: " + treeWalk.getPathString() + " SHA: " + treeWalk.getObjectId(0));
				set.put(treeWalk.getObjectId(0), treeWalk.getPathString());
			}
		}
		
		treeWalk.close();
		
		return set;
	}
	

}
