package edu.serene.extraction;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ParseException;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.BodyDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.TypeDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

public class ClassWalker extends VoidVisitorAdapter<Object> {

	public HashMap<String, String> methodSet = new HashMap<String, String>();
	private CompilationUnit cu;
	public ClassWalker(File file) throws ParseException, IOException {
		FileInputStream in = new FileInputStream(file);
		
	    try {
	        // parse the file
	        cu = JavaParser.parse(in);
	    } finally {
	        in.close();
	    }
	}
	
	public void traverse() {
		visit(cu, null);
	}
	
	public void visit(CompilationUnit n, Object arg) {
        super.visit(n, arg);
        //System.out.println(n.getName());
        List<TypeDeclaration> types = n.getTypes();
        String packageName = n.getPackage().toString().replaceAll("package", "").replace(";", "").replace("\n", "");
        
        for(TypeDeclaration type : types) {
        	if(type instanceof ClassOrInterfaceDeclaration) {		
        			walkClass(packageName, (ClassOrInterfaceDeclaration)type);
        	}
        }
   }
	
	public void walkClass(String packageName, ClassOrInterfaceDeclaration classDef) {
	
		List<BodyDeclaration> members = classDef.getMembers();
		boolean printHeader = true;
		for (BodyDeclaration member : members) {
		
			if (member instanceof MethodDeclaration) {
				MethodDeclaration methDec = (MethodDeclaration) member;
				methodSet.put(classDef.getName() + " / " + methDec.getDeclarationAsString(), methDec.getBody().toString());
        	}
			
			if(member instanceof ClassOrInterfaceDeclaration) {
				//This is a nested class, need to walk its entire tree as well...
				walkClass(packageName, (ClassOrInterfaceDeclaration)member);
			}
		}
	}
}
