package edu.serene.extraction;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.github.javaparser.ParseException;

public class CompilationUnitComparer {

	public ArrayList<String> changedMethods = new ArrayList<String>();
	public CompilationUnitComparer(File left, File right) throws ParseException, IOException {
		ClassWalker leftWalker = new ClassWalker(left);
		leftWalker.traverse();
		
		ClassWalker rightWalker = new ClassWalker(right);
		rightWalker.traverse();
		
		Iterator<Entry<String, String>> it = leftWalker.methodSet.entrySet().iterator();
		
		while(it.hasNext()) {
			Map.Entry<String, String> entry = (Map.Entry<String, String>) it.next();
			String methodBody = rightWalker.methodSet.get(entry.getKey());
			if(methodBody != null) {
				if(!methodBody.trim().equals(entry.getValue().trim())) {
					changedMethods.add(entry.getKey());
				}
			}
		}
	}
}
