package edu.serene.bean;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jgit.revwalk.RevCommit;

public class IssueBean {

	private String commit;
	private String number;
	
	public IssueBean (String commitHash, String issueNumber) {
		number = issueNumber;
		commit = commitHash;
	}
	
	public IssueBean (RevCommit rc) {
		commit = rc.getName();
		number = getLinkedIssue(rc.getShortMessage());
	}
	
	public IssueBean () {
		
	}
	
	public String getCommit() {
		return commit;
	}
	
	public void setCommit(String c) {
		commit = c;
	}
	
	public String getNumber() {
		return number;
	}
	
	public void setNumber(String n) {
		number = n;
	}
	
	private static String getLinkedIssue(String message) {
		if(message == null) {
			return null;
		}
		String issueNumberPattern = "#\\d+";
		String stdMessage = message.toLowerCase();
		Pattern regex = Pattern.compile(issueNumberPattern);
		
		Matcher match = regex.matcher(stdMessage);
		if(match.find()) {
			if(stdMessage.contains("issue") || stdMessage.contains("bug")) {
				return match.group(0);
			}
		} 
		
		return "";
	}
}
