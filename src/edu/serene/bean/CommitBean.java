package edu.serene.bean;

import java.util.ArrayList;

public class CommitBean {

	private String sha;
	
	public String getSha() {
		return sha;
	}
	
	private ArrayList<String> changedMethods;
	
	public ArrayList<String> getChangedMethods() {
		return changedMethods;
	}
	
	public CommitBean(String id) {
		sha = id;
	}
	
	public void updateChangedMethods(String oldClassFile, String newClassFile) {
		
	}
}
